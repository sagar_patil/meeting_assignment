from flask import Flask, make_response, jsonify, json, request, g
#import config, time, traceback, uuid, pytz
from time import strftime
from app.resources.controllers import resources
from datetime import datetime, timezone
from flask_compress import Compress
from flask_sqlalchemy import SQLAlchemy, get_state
from flask_cors import CORS, cross_origin
from app.database import *


app = Flask(__name__)



Compress(app)


app.register_blueprint(resources, url_prefix='/api/v1')


@app.errorhandler(404)
def not_found(error):
    print('404 error for url:', request.url)
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.errorhandler(500)
def internal_error(exception):
    # logger.error(exception)
    print('Error handler => '+str(exception))
    return make_response(jsonify({'error': 'Server Fatal Error! Kindly Hang on!'}), 404)


@app.errorhandler(Exception)
def unhandled_exception(exception):
    print('Unhandled Error handler => ' + str(exception))
    return make_response(jsonify({'error': 'Server Fatal Error! Kindly Hang on!!'}), 404)


@app.before_request
def before_request_func():
    print("request recieved")
