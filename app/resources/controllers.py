import json

from cerberus import Validator
from flask import request
from flask_restful import Resource
# from datetime import datetime
from flask import Blueprint, jsonify, request, make_response, g
from app.utils import *
import hmac
import hashlib
import base64
import config
from app import helpers
from app.helpers import *


resources = Blueprint('', __name__,)
v = Validator()
meeting_data = json.load(open("./docs/schema/meetingSchema.json"))


v = Validator()

@resources.route('/meetings', methods=['POST'])
def schedule_meeting():
    #import pdb;pdb.set_trace()
    """" This is a Post API
    Created to schedule meetings based on participants availabilityself.
    if already a meeting scheduled before for a participant then API will not overwrite the Existing Meeting

    """
    response_code, response_data, err_msg, message = initialize_values()
    body = request.get_json()
    res =v.validate(body, meeting_data["meeting_schema"])
    if not res:
            response_code = 400
            err_msg = v.errors
            return send_response(response_code, response_data, err_msg, message=message)
    from datetime import datetime as dt
    start_time = dt.strptime(body.get('start_time'), "%Y-%m-%d %H:%M:%S")
    end_time = dt.strptime(body.get('end_time'), "%Y-%m-%d %H:%M:%S")
    start_time = datetime.strptime(body.get('start_time'), "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(body.get('end_time'), "%Y-%m-%d %H:%M:%S")
    if end_time < start_time:
        err_msg = "Invalid start and end time"
        response_code = 404
        return send_response(response_code, response_data, err_msg, message=message)
    try:
        response_code, response_data, err_msg, message = helpers.meeting_scheduler(
            body, response_code, response_data, err_msg, message)

        if response_code==200:
            message="meeting scheduled successfully."
            return send_response(response_code, response_data, err_msg, message=message)
        else:
            return send_response(response_code, response_data, err_msg, message=message)

    except Exception as e:
        response_code, err_msg = handle_exception(e)
        message = 'Something went wrong! Please try after sometime!'
        return send_response(response_code, response_data, err_msg, message=message)

@resources.route('/meeting/<ID>', methods=['GET'])
def get_meetings(ID):
    """" This is a GET API
    This API fetch meeting info based on meetingID
    return the meeting details in json format .

    """
    error_code = 400
    try:
        response_code, response_data, err_msg, message = initialize_values()

        if not ID:
                response_code = 400
                err_msg = "Meeting id missing."
                return send_response(response_code, response_data, err_msg, message=message)
        try:
            response_code, response_data, err_msg, message = helpers.get_meeting_details(
                ID, response_code, response_data, err_msg, message)

            if response_code==200:
                message="meeting data fetched successfully."
                return send_response(response_code, response_data, err_msg, message=message)
            else:
                return send_response(response_code, response_data, err_msg, message=message)

        except Exception as e:
            response_code, err_msg = handle_exception(e)
            message = 'Something went wrong! Please try after sometime!'
            #return send_response(response_code, response_data, err_msg, message=message)
    except Exception as e:
        response_code, err_msg = handle_exception(e)
        message = 'Something went wrong! Please try after sometime!'
        return send_response(response_code, response_data, err_msg, message=message)

@resources.route('/meetings', methods=['GET'])
def get_meetings_info():
    """" This is a GET API
    This API fetch meeting info based on participant_email_id
    and start_time and end_time
    return the meeting details in json format .

    """
    error_code = 400
    try:
        response_code, response_data, err_msg, message = initialize_values()

        if request.args.get('participant'):
                body={"participant_email_id":request.args.get('participant')}
        elif  request.args.get('start_time') and  request.args.get('end_time'):
            body={"start_time":request.args.get('start_time'),"end_time":request.args.get('end_time')}
        else:
            response_code = 400
            err_msg = "Required parameters to fetch meeting info are missing."
            return send_response(response_code, response_data, err_msg, message=message)

        try:
            response_code, response_data, err_msg, message = helpers.get_meeting_details(
                body, response_code, response_data, err_msg, message)

            if response_code==200:
                message="meeting data fetched successfully."
                return send_response(response_code, response_data, err_msg, message=message)
            else:
                return send_response(response_code, response_data, err_msg, message=message)

        except Exception as e:
            response_code, err_msg = handle_exception(e)
            message = 'Something went wrong! Please try after sometime!'
            return send_response(response_code, response_data, err_msg, message=message)
    except Exception as e:
        response_code, err_msg = handle_exception(e)
        message = 'Something went wrong! Please try after sometime!'
        return send_response(response_code, response_data, err_msg, message=message)
