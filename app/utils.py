import sys,re
import traceback
from app import *
import config
import uuid
def initialize_values():
    # response_code, response_data, err_msg, message
    return 200, '', '', ''

def generate_uuid():
    return str(uuid.uuid4())


def handle_exception(e):
    if len(e.args) > 0:
        err_msg = e.args[0]
        if len(e.args) > 1:
            response_code = e.args[1]
        else:
            response_code = 400
    else:
        err_msg = 'Something wrong with server. Kindly Contact Admin'
        response_code = 400
    return response_code, err_msg


def send_response(response_code, response_data=None, err_msg=None, extra_response=False, message=None):
    try:
        ##import pdb;pdb.set_trace()
        if response_code == 200 or response_code == 201 or response_code == 202 or response_code == 204:
            if extra_response:
                response = {'data': response_data, 'meta_data': extra_response}
            else:
                response = {'data': response_data}
            success = True
        else:
            if callable(err_msg):
                err_msg = constants.GENERAL_RESPONSE[response_code]
            response = {'error': err_msg}
            success = False
        if not response_data:
            response_data = None
        if not err_msg:
            err_msg = None
        structured_response = {
            'success': success,
            'status': response_code,
            'data': response_data,
            'errors': err_msg,
            'message': message
        }
        return make_response(jsonify(**structured_response), response_code)
    except Exception as e:
        response_code = 400
        response = {
            'success': False,
            'status': response_code,
            'data': None,
            'errors': str(e),
            'message': 'Server Error. Kindly hold on.'
        }
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
        return make_response(jsonify(**response), response_code)
