from sqlalchemy import Column, Integer, String, DateTime, Date, DECIMAL, JSON, Boolean, ForeignKey, func,Enum
from sqlalchemy.orm import relationship
from app.database import Base
import os

from datetime import datetime
import uuid
import time
from pytz import timezone
from sqlalchemy import exc as sqlexc
from sqlalchemy import desc
#import logging
from app.database import *
from app import helpers
from app.database import db
import config



class Meetings(db.Model):
    __tablename__ = "meetings"

    Id = db.Column("Id", String(64), primary_key=True)
    meeting_id= db.Column("meeting_id", String(64), nullable=False)
    #CreatedDate =db.Column("CreatedDate", DateTime, nullable=False)
    participant_email_id=Column("email", String(128), nullable=False)
    participant_name=Column("participant_name", String(128), nullable=False)
    title =db.Column("title", String(128), nullable=False)
    start_time =db.Column("start_time",DateTime, nullable=False)
    end_time =db.Column("end_time",DateTime, nullable=False)
    RSVP =db.Column("RSVP",Enum("Yes", "No","ServiceProvider"),
                       nullable=False)
    creation_timestamp =db.Column("creation_timestamp", DateTime,nullable=True)

    def __repr__(self):
        return "<meetings(Id='{}',meeting_id='{}')>".format(self.Id, self.participant_email_id)

    def get_json(self):
        body = {
        "RSVP":self.RSVP,
            "meeting_id": self.meeting_id,
            "title": self.title,
            "start_time": self.start_time,
            "end_time": self.end_time,
            "creation_timestamp": self.creation_timestamp,
            "participant_email_id":self.participant_email_id,
            "participant_name":self.participant_name
        }
        return body
