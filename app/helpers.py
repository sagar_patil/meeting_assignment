from datetime import datetime as dt
import json
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import load_only
from sqlalchemy import func, distinct
from datetime import datetime
from app.database import db_session
from app.utils import initialize_values

import app.models as md
from app.utils import *




def meeting_scheduler(json_dict, response_code, response_data, err_msg, message):
    try:
        session = db_session.query(md.Meetings)

        meetings=session.filter(md.Meetings.start_time >=json_dict['start_time'], md.Meetings.end_time <=json_dict['end_time'])

        scheduled_meetings=[]

        for i in meetings:
            scheduled_meetings.append(i.participant_email_id)

        for i in json_dict['participants']:
            if i['email_id'] in scheduled_meetings:
                err_msg="Meeting are overlapping for the participant"
                message="Can not schedule meeting"
                response_code=400
                return  response_code, response_data, err_msg, message


        meeting_id =generate_uuid()
        for i in json_dict['participants']:
            user=md.Meetings()
            user.Id = generate_uuid()
            user.archived = False
            user.meeting_id = json_dict.get('meeting_id',meeting_id)
            user.start_time=json_dict['start_time']
            user.end_time=json_dict['end_time']
            user.creation_timestamp=json_dict.get(creation_timestamp,datetime.datetime.now())
            user.RSVP =i['RSVP']
            user.participant_email_id=i['email_id']
            user.participant_name=i['participant_name']
            user.title=json_dict['title']

            ret = db_session.add(user)
            try:
                ret = db_session.commit()
            except SQLAlchemyError as sql_error:
                db_session.rollback()
                response_code = 400
                err_msg = str(sql_error)
                message = "Exception While adding viewed properties details to the DB"
    except Exception as e:
        response_code, err_msg = handle_exception(e)
        message = 'Something went wrong! Please try after sometime!'
        return response_code, response_data, err_msg, message



def get_meeting_details(body, response_code, response_data, err_msg, message):
    try:
        session = db_session.query(md.Meetings)
        if 'ID' in body:
            meeting_details =session.filter_by(meeting_id=body['ID'],RSVP="Yes").all()
        elif 'start_time' and 'end_time' in body:
            meeting_details=session.filter(md.Meetings.start_time >=body['start_time'], md.Meetings.end_time <=body['end_time'])
        elif 'participant_email_id' in body:
            meeting_details =session.filter_by(participant_email_id=body['participant_email_id']).all()




        if meeting_details and 'participant_email_id' not in body:
            participant_info=[]
            for i in meeting_details:
                    i=i.get_json()
                    participant_info.append({"Name":i['participant_name'],"email":i['participant_email_id'],"RSVP":i['RSVP']})
            response_data={"meeting_id":meeting_details[0].Id,"title":meeting_details[0].title,"start_time":meeting_details[0].start_time,"creation_timestamp":meeting_details[0].creation_timestamp,"end_time":meeting_details[0].end_time,"participants":participant_info}
        elif meeting_details and 'participant_email_id' in body:
            meeting_info=[]
            for i in meeting_details:
                    i=i.get_json()
                    meeting_info.append({"title":i['title'],"start_time":i['start_time'],"RSVP":i['RSVP'],"end_time":i['end_time'],"meeting_id":i['meeting_id']})
            response_data={"name":meeting_details[0].participant_name,"email_id":meeting_details[0].participant_email_id,"meeting_info":meeting_info}

    except Exception as e:
        response_code=200
        err_msg = "Couldn't fetch meeting info"
        message = 'No meeting data found.'
    return response_code, response_data, err_msg, message
