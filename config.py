# Statement for enabling the development environment
DEBUG = True

# Define the application directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
# Define the database - we are working with
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@123@localhost/Meeting_assignment'
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}
SQLALCHEMY_ECHO=False
SQLALCHEMY_POOL_RECYCLE=3600

THREADS_PER_PAGE = 2
